
# Redis

В некотором роде хорошим описанием Redis является Remote Dictionary Service. Это значит, что мы можем хранить в нем большие словари (но значительно меньшие чем в обычной бд), к которым осуществляется большое количество запросов и которые нужно использовать постоянно, сразу с нескольких сервисов и нужна большая скорость для получения таких данных.

## Docker

Образ: https://hub.docker.com/_/redis

Установка Redis в чистом виде как приложения не имеет особого смысла, так как там просто мы скачиваем архив, распаковываем, запускаем make.

Интереснее организовать Docker-контейнер, так как это больше похоже на реальность.

Пример простого docker-compose в котором мы задаем пароль и открываем для подключения порт:

```yaml
Redis:
  image: "redis"
  command: "redis-server --requirepass ${REDIS_PASS}"
  ports:
    - "6379:6379"
```

Для общения из командной строки используется redis-cli, который в apt входит в пакет redis-tools:

```
sudo apt-get install redis-tools
redis-cli --version
```

Подключение к базе через cli:

```
redis-cli -h localhost -p 6379 -a password
```

## Примеры команд

Самая простая команда для проверки подключения:

```
localhost:6379> PING
PONG
```

Завести новую запись:

```
localhost:6379> set test-key test-value
OK
```

Получить значение по ключу:

```
localhost:6379> get test-key
"test-value"
```

Создание/получение нескольких записей одновеременно:

```
localhost:6379> MSET a a-value b b-value c c-value
OK
localhost:6379> MGET a b c d
1) "a-value"
2) "b-value"
3) "c-value"
4) (nil)
```

(nil) - аналог None/null. в данном случае сигнализирует

Проверка на существование ключа:

```
localhost:6379> EXISTS a
(integer) 1
localhost:6379> EXISTS d
(integer) 0
```

Создание/вывод хеш-словаря (аналог обычного словаря):

```
localhost:6379> HSET dict a 0
(integer) 1
localhost:6379> HSET dict b 2
(integer) 1
localhost:6379> HSET dict c 4
(integer) 1
localhost:6379> HGET dict a
"0"
localhost:6379> HGET dict b
"2"
```

Задание одновременно большого числа подполей хеша и получение всех ключей-значений:

```
localhost:6379> HMSET dict d 6 e 8
OK
localhost:6379> HGETALL dict
 1) "a"
 2) "0"
 3) "b"
 4) "2"
 5) "c"
 6) "4"
 7) "d"
 8) "6"
 9) "e"
10) "8"
```

Сеты - это коллекции уникальных строк:
```
redis 127.0.0.1:6379> SADD tutorials redis
(integer) 1
redis 127.0.0.1:6379> SADD tutorials mongodb
(integer) 1
redis 127.0.0.1:6379> SADD tutorials mysql
(integer) 1
redis 127.0.0.1:6379> SADD tutorials mysql
(integer) 0
redis 127.0.0.1:6379> SMEMBERS tutorials
1) "mysql"
2) "mongodb"
3) "redis"
```

Лист - это массив отсортированный путем метода вставки элементов, вот например вставка элементов в начало массива (добавление в конец - RPUSH):
```
redis 127.0.0.1:6379> LPUSH tutorials redis
(integer) 1
redis 127.0.0.1:6379> LPUSH tutorials mongodb
(integer) 2
redis 127.0.0.1:6379> LPUSH tutorials mysql
(integer) 3
redis 127.0.0.1:6379> LRANGE tutorials 0 10
1) "mysql"
2) "mongodb"
3) "redis"
```

Короткий список команд для разных типов данных:

| type    | commands                                                                                                                                                                                   |
| ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Sets    | SADD, SCARD, SDIFF, SDIFFSTORE, SINTER, SINTERSTORE, SISMEMBER, SMEMBERS, SMOVE, SPOP, SRANDMEMBER, SREM, SSCAN, SUNION, SUNIONSTORE                                                       |
| Hashes  | HDEL, HEXISTS, HGET, HGETALL, HINCRBY, HINCRBYFLOAT, HKEYS, HLEN, HMGET, HMSET, HSCAN, HSET, HSETNX, HSTRLEN, HVALS                                                                        |
| Lists   | BLPOP, BRPOP, BRPOPLPUSH, LINDEX, LINSERT, LLEN, LPOP, LPUSH, LPUSHX, LRANGE, LREM, LSET, LTRIM, RPOP, RPOPLPUSH, RPUSH, RPUSHX                                                            |
| Strings | APPEND, BITCOUNT, BITFIELD, BITOP, BITPOS, DECR, DECRBY, GET, GETBIT, GETRANGE, GETSET, INCR, INCRBY, INCRBYFLOAT, MGET, MSET, MSETNX, PSETEX, SET, SETBIT, SETEX, SETNX, SETRANGE, STRLEN |

Также существуют и более замысловатые типы данных такие как: geospatial items, sorted sets, и HyperLogLog.

Отчистить базу:

```
localhost:6379> FLUSHDB
OK
```

## Python

Python-драйвер: https://pypi.org/project/redis/

Подключение Redis:

```python
import os
import redis

REDISDB = redis.Redis(
    host=os.environ.get("REDIS_HOST"),
    port=os.environ.get("REDIS_PORT"),
    password=os.environ.get("REDIS_PASS")
)
```

В целом общение с Redis в python достаточно интуитивно:

```python
REDISDB.mset({
    "key-1": 0,
    "key-2": "value"
})

print(REDISDB.get("key-1"))
b'0'
```

Стоит иметь в виду следующие особенности Redis:
- самый меньший тип данных с которым оперирует Redis - это string, все остальные типы данных грубо говоря являются контейнерами над этим типом данных
- представление строки, возвращаемое в python от Redis являются битовым представлением строки, то есть для использование требуется декодировать:
    ```
    print(REDISDB.get("key-1").decode('utf-8'))
    0
    ```


Читать:
- https://realpython.com/python-redis/
