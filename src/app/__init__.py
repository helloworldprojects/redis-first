from config import REDISDB


def start():
    REDISDB.mset({
        "key-1": 0,
        "key-2": "value"
    })

    print(REDISDB.get("key-1").decode('utf-8'))
